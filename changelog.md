# Changelog
### 1.0.4
* Updated mod for Avorion 1.0

### 1.0.3
* Updated mod for Avorion 0.29
### 1.0.2
* Added compatibility for Avorion 0.26.1 and higher

### 1.0.1
* Added compatibility for Avorion 0.24.x

### 1.0.0
* Create project

