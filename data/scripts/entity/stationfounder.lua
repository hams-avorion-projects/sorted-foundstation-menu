package.path = package.path .. ";data/scripts/lib/?.lua"

include ("extutils")




function StationFounder.buildMiscStationGui(tab)
    -- make levels a table with key == value

    -- create background
    local frame = tab:createScrollFrame(Rect(vec2(), tab.size))
    frame.scrollSpeed = 40
    frame.paddingBottom = 17
	
	
	-- Provide backwards compatibility for Avorion 0.23.x
	-- May be removed when 0.24.x is on stable branch
	if not StationFounder.stations then
		StationFounder.stations = stations
		StationFounder.stationsByButton = stationsByButton
	end

	

    local count = 0
    for index, station in spairs(StationFounder.stations, function(t,a,b) return StationFounder.stations[b].name > StationFounder.stations[a].name end) do

        local stationName = station.name

        local padding = 10
        local height = 30
        local width = frame.size.x - padding * 4

        local lower = vec2(padding, padding + ((height + padding) * count))
        local upper = lower + vec2(width, height)

        local rect = Rect(lower, upper)

        local vsplit = UIVerticalSplitter(rect, 10, 0, 0.8)
        vsplit.rightSize = 100

        local button = frame:createButton(vsplit.right, "Transform"%_t, "onFoundStationButtonPress")
        button.textSize = 16
        button.bold = false

        frame:createFrame(vsplit.left)

        vsplit = UIVerticalSplitter(vsplit.left, 10, 7, 0.7)

        local label = frame:createLabel(vsplit.left.lower, stationName, 14)
        label.size = vec2(vsplit.left.size.x, vsplit.left.size.y)
        label:setLeftAligned()

        label.tooltip = station.tooltip or ""

        local costs = StationFounder.getStationCost(station)

        local label = frame:createLabel(vsplit.right.lower, createMonetaryString(costs) .. " Cr"%_t, 14)
        label.size = vec2(vsplit.right.size.x, vsplit.right.size.y)
        label:setRightAligned()

        StationFounder.stationsByButton[button.index] = index

        count = count + 1
    end

end

function StationFounder.buildFactoryGui(levels, tab)

    -- make levels a table with key == value
    local l = {}
    for _, v in pairs(levels) do
        l[v] = v
    end
    levels = l

    -- create background
    local frame = tab:createScrollFrame(Rect(vec2(), tab.size))
    frame.scrollSpeed = 40
    frame.paddingBottom = 17

    local usedProductions = {}
    local possibleProductions = {}

    for good, productions in pairs(productionsByGood) do

        for index, production in ipairs(productions) do
            -- mines shouldn't be built just like that, they need asteroids
            if not production.mine then

                -- read data from production
                local result = goods[production.results[1].name];

                -- only insert if the level is in the list
                if good == production.results[1].name then
                    if levels[result.level] ~= nil and not usedProductions[production.index] then
                        usedProductions[production.index] = true
                        table.insert(possibleProductions, {production=production, index=index})
                    end
                end
            end
        end
    end

    local comp = function(a, b)
        local nameA = a.production.factory
        if a.production.fixedName == false then
            nameA = a.production.results[1].name%_t .. " " .. nameA%_t
        end

        local nameB = b.production.factory
        if b.production.fixedName == false then
            nameB = b.production.results[1].name%_t .. " " .. nameB%_t
        end

        if nameA == nameB then
            return a.production.index < b.production.index
        end

        return nameA < nameB
    end

    table.sort(possibleProductions, comp)

    local count = 0
    for _, p in spairs(possibleProductions, function(t,a,b) return getTranslatedFactoryName(possibleProductions[b].production) > getTranslatedFactoryName(possibleProductions[a].production) end) do

        local production = p.production
        local index = p.index

        local result = goods[production.results[1].name];
        local factoryName = getTranslatedFactoryName(production)

        local padding = 10
        local height = 30
        local width = frame.size.x - padding * 4

        local lower = vec2(padding, padding + ((height + padding) * count))
        local upper = lower + vec2(width, height)

        local rect = Rect(lower, upper)

        local vsplit = UIVerticalSplitter(rect, 10, 0, 0.8)
        vsplit.rightSize = 100

        local button = frame:createButton(vsplit.right, "Transform"%_t, "onFoundFactoryButtonPress")
        button.textSize = 16
        button.bold = false

        frame:createFrame(vsplit.left)

        vsplit = UIVerticalSplitter(vsplit.left, 10, 7, 0.7)

        local label = frame:createLabel(vsplit.left.lower, factoryName, 14)
        label.size = vec2(vsplit.left.size.x, vsplit.left.size.y)
        label:setLeftAligned()

        local tooltip = "Produces:\n"%_t
        for i, result in pairs(production.results) do
            if i > 1 then tooltip = tooltip .. "\n" end
            tooltip = tooltip .. " - " .. result.name%_t
        end


        local first = 1
        for _, i in pairs(production.ingredients) do
            if first == 1 then
                tooltip = tooltip .. "\n\n" .. "Requires:"%_t
                first = 0
            end
            tooltip = tooltip .. "\n - " .. i.name%_t
        end
        label.tooltip = tooltip

        local costs = getFactoryCost(production)

        local label = frame:createLabel(vsplit.right.lower, createMonetaryString(costs) .. " Cr"%_t, 14)
        label.size = vec2(vsplit.right.size.x, vsplit.right.size.y)
        label:setRightAligned()


		-- Provide backwards compatibility for Avorion 0.23.x
		-- May be removed when 0.24.x is on stable branch
		if not StationFounder.productionsByButton then
			StationFounder.productionsByButton = productionsByButton
		end
		
		
        StationFounder.productionsByButton[button.index] = {goodName = result.name, factory=factoryName, index = index, production = production}

        count = count + 1


    end
end